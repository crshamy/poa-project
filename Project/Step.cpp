#include "Step.h"

vector<Ingredient> Step::GetRawIngredients() const
{
	vector<Ingredient> _rawIngredients = vector<Ingredient>();
	for (const Ingredient& _ingredient : mIngredients)
		if (_ingredient.IsRaw()) _rawIngredients.push_back(_ingredient);
	return _rawIngredients;
}

Step::Step(){}

Step::Step(const vector<Ingredient>& _ingredients, const Mode& _mode, const Ingredient& _result) : mIngredients(_ingredients), mMode(_mode), mResult(_result){}

Ingredient Step::DoStep() const
{
	string _feedback = "\n";
	switch (mMode) 
	{
		case Mode::Slice:
		_feedback += "Slicing :\n";
		break;

		case Mode::Bake:
		_feedback += "Baking :\n";
		break;

		case Mode::Mix:
		_feedback += "Mixing :\n";
		break;

		case Mode::Default:
		break;
	}
	for (const Ingredient& _ingredient : mIngredients)
		_feedback += _ingredient.ToString()+"\n";
	_feedback += "Receiving : " + mResult.ToString();
	cout << _feedback << endl;
	cout << endl;
	return mResult;
}
