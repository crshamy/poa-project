#pragma once

#define SIZE_GRID 5
#define BLANK_CHAR '.'
#define ROBOT_CHAR 'r'

#define WORKBENCH "Workbench"

#define SALAD "Salad"
#define TOMATO "Tomato"
#define OLIVE "Olive"
#define ONION "Onion"
#define MEAT "Meat"
#define BREAD "Bread"

#define SLICED_SALAD "Sliced Salad"
#define SLICED_TOMATO "Sliced Tomato"
#define SLICED_ONION "Sliced Onion"
#define SLICED_MEAT "Sliced Meat"

#define MIXED_SALAD_TOMATO "Mixed Tomato Salad"
#define MIXED_SALAD_TOMATO_OLIVE "Mixed Tomato Salad Olive"

#define BAKED_MEAT "Baked Meat"

#define KEBAB "Kebab"
