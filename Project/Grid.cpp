#include "Grid.h"
#include "Ingredient.h"

string Grid::GetSpotAtPos(const Vec2& _position)
{
	for (pair<string, vector<Vec2>> _pair : mImportantSpots)
	{
		if (_pair.second.size() == 0) continue;
		for (Vec2 _positionSpot : _pair.second)
			if (_position == _positionSpot) return _pair.first;
	}
	return "";
}

Grid::Grid(const unsigned int& _dimension) : mDimension(_dimension)
{
	mGrid = new Node[_dimension* _dimension];
	for (unsigned int _indexLine = 0; _indexLine < _dimension; ++_indexLine)
		for (unsigned int _indexColumn = 0; _indexColumn < _dimension; ++_indexColumn)
			mGrid[_indexLine * mDimension + _indexColumn] = Node(_indexLine, _indexColumn);
}

Grid::~Grid()
{
	delete[] mGrid;
}

bool Grid::ContainsStock(const Vec2& _position) const
{
	bool _contains = false;
	for (const pair<unsigned int, Ingredient>& _stock : mStocks)
	{
		_contains |= _stock.first == GetInArrayPosition(_position.X, _position.Y);
		if (_contains) break;
	}
	return _contains;
}

bool Grid::ContainsSpot(const string& _nameSpot) const
{
	bool _contains = false;
	for (const pair<string, vector<Vec2>>& _spot : mImportantSpots)
	{
		_contains |= _spot.first == _nameSpot;
		if (_contains) break;
	}
	return _contains;
}

bool Grid::ContainsPosition(const Vec2& _position, const vector<Vec2>& _positions) const
{
	bool _contains = false;
	for (const Vec2& _positionInVec : _positions)
	{
		_contains |= _position == _positionInVec;
		if (_contains) break;
	}
	return _contains;
}

void Grid::SetStock(const unsigned int& _line, const unsigned int& _column, const Ingredient& _ingredient)
{
	if (_line >= mDimension || _column >= mDimension) throw out_of_range("/!\\Stock : Line or Column out of range/!\\");
	const Vec2& _position = Vec2(_line, _column);
	if (ContainsStock(_position)) ClearStockAtPos(_line, _column);
	mStocks[GetInArrayPosition(_line, _column)] = _ingredient;
	SetImportantSpot(_line, _column, _ingredient.GetName());
}

void Grid::ClearStockAtPos(const unsigned int& _line, const unsigned int& _column)
{
	const Vec2& _position = Vec2(_line, _column);
	if(!ContainsStock(_position)) return;
	map<unsigned int, Ingredient> _newStock = map<unsigned int, Ingredient>();
	for (const pair<unsigned int, Ingredient>& _pair : mStocks)
		if (_pair.first != GetInArrayPosition(_line, _column)) _newStock[_pair.first] = _pair.second;
	mStocks = _newStock;
	ClearSpotAtPos(_line, _column);
}

void Grid::SetImportantSpot(const unsigned int& _line, const unsigned int& _column, const string& _spotName)
{
	const unsigned int& _index = _line * mDimension + _column;
	if (_line >= mDimension || _column >= mDimension) throw out_of_range("/!\\Spot : Line or Column out of range/!\\");
	Node* _oldNode = &mGrid[_index];
	if (_oldNode && _oldNode->GetName() != _spotName) 
	{
		ClearSpotAtPos(_line, _column);
		*_oldNode = Node(_line, _column, _spotName);
	}
	vector<Vec2>& _spotPositions = mImportantSpots[_spotName];
	const Vec2& _position = Vec2(_line, _column);
	if(!ContainsPosition(_position, _spotPositions))_spotPositions.push_back(_position);
}

void Grid::ClearSpotAtPos(const unsigned int& _line, const unsigned int& _column)
{
	Node* _nodeToClear = &mGrid[_line * mDimension + _column];
	const Vec2& _positionToClear = Vec2(_line, _column);
	for (const pair<string, vector<Vec2>>& _spot : mImportantSpots)
	{
		if (_spot.first == _nodeToClear->GetName())
		{
			const vector<Vec2>& _oldSpots = _spot.second;
			vector<Vec2> _newSpots = vector<Vec2>(_oldSpots.size() - 1);
			unsigned int _spotIndex = 0;
			for (const Vec2& _position : _oldSpots)
			{
				if (_position == _positionToClear) continue;
				_newSpots[_spotIndex] = _position;
				++_spotIndex;
			}
			mImportantSpots[_spot.first] = _newSpots;
			break;
		}
	}
	*_nodeToClear = Node(_positionToClear);
}

void Grid::DisplayStocks() const
{
	string _listStocks = "Stocks :\n";
	for (const pair<unsigned int, Ingredient>& _stock : mStocks)
	{
		const Vec2& _position = Vec2(_stock.first / mDimension, _stock.first % mDimension);
		_listStocks += "At position : " + _position.DisplayVector()+ " there is " + _stock.second.ToString() +'\n';
	}
	cout << _listStocks << endl;
}

void Grid::DisplayImportantSpots() const
{
	string _listSpots = "Spots :\n";
	for (const pair<string, vector<Vec2>>& _spot : mImportantSpots)
	{
		_listSpots += _spot.first + " : {";
		const unsigned int& _size = _spot.second.size();
		if (_size >= 0) _listSpots += _spot.second[0].DisplayVector();
		for (unsigned int _index = 1; _index < _size; ++_index)
			_listSpots += '|' + _spot.second[_index].DisplayVector();
		_listSpots += "}\n";
	}
	cout << _listSpots << endl;
}

void Grid::DisplayGrid(const Vec2& _robotPosition) const
{
	string _gridDisplay = "\nGrid :\n";
	for (unsigned int _indexNode = 0; _indexNode < mDimension * mDimension; ++_indexNode)
	{
		Node _node = mGrid[_indexNode];
		if(_node.GetName().empty() && _node.GetPosition() == _robotPosition) _gridDisplay += ROBOT_CHAR;
		else if (_node.GetPosition() == _robotPosition) _gridDisplay += _node.DisplayNode() + ROBOT_CHAR;
		else _gridDisplay += _node.DisplayNode();
		_gridDisplay += ((_indexNode + 1) % mDimension == 0) ? '\n' : ' ';
	}
	cout << _gridDisplay << endl;
}
