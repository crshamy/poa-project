#include "Receipt.h"

Receipt::Receipt(){}

Receipt::Receipt(const string& _name, const vector<Step>& _steps) : mName(_name), mSteps(_steps){}

string Receipt::DisplayReceipt() const
{
	return mName + " : " + DisplayListIngredients();
}

map<string, Ingredient> Receipt::GetAllRawIngredients() const
{
	map<string, Ingredient> _allRawIngredients = map<string, Ingredient>();

	for (const Step& _step : mSteps)
	{
		vector<Ingredient> _rawIngredientForThisStep = _step.GetRawIngredients();
		for (const Ingredient& _rawIngredient : _rawIngredientForThisStep) 
		{
			const string& _name = _rawIngredient.GetName();
			Ingredient& _rawIngredientInList = _allRawIngredients[_name];
			if (_rawIngredientInList.GetName().empty()) _rawIngredientInList.SetName(_name);
			_rawIngredientInList.IncreaseAmount(_rawIngredient.GetQuantity());
		}
	}
	return _allRawIngredients;
}

string Receipt::DisplayListIngredients() const
{
	string _ingredients = "";
	map<string, Ingredient> _allRawIngredients = GetAllRawIngredients();

	for (pair<string, Ingredient> _pairIngredientQuantity : _allRawIngredients)
		_ingredients += _pairIngredientQuantity.first+" ";
	_ingredients += "\n";

	return _ingredients;
}
