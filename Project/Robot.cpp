#include "Robot.h"
#include "Receipt.h"
#include "Grid.h"

#include "Macro.h"

Robot::Robot(){}

Robot::Robot(const vector<Receipt>& _receipts) : mReceipts(_receipts){}

void Robot::UpdateRobot()
{
	switch (mRobotState)
	{
	case State::PROPOSING:
		break;

	case State::ANALYZING:
		if (!mWorkspace)
		{
			UpdateRobotState(State::SHUTTING_DOWN);
			break;
		}
		AnalyzeEnvironnement();
		UpdateRobotState(State::SEARCHING);
		break;

	case State::SEARCHING:
		//const bool& _noMoreIngredientNeeded = (mIndexIngredientToSearch>= mNeededIngredients.size());
		if(mIndexIngredientToSearch>= mNeededIngredients.size())ChooseNearestPosition(WORKBENCH);
		else ChooseNearestPosition(mNeededIngredients[mIndexIngredientToSearch].GetName());
		UpdateRobotState(State::MOVING);
		break;

	case State::MOVING:
		MoveToTarget();
		if (mCurrentPostion != mTargetPosition) break;
		if(mWorkspace->GetSpotAtPos(mCurrentPostion) == WORKBENCH) UpdateRobotState(State::COOKING);
		else UpdateRobotState(State::GATHERING);
		break;

	case State::GATHERING:
		TakeFromStock(*mWorkspace->GetStockAtPos(mCurrentPostion));
		if (!HasEnough(mNeededIngredients[mIndexIngredientToSearch].GetName(), mNeededIngredients[mIndexIngredientToSearch].GetQuantity())) break;
		++mIndexIngredientToSearch;
		UpdateRobotState(State::SEARCHING);
		break;

	case State::COOKING:
		DoCurrentReceipt();
		break;

	case State::SHUTTING_DOWN:
		mbIsLaunched = false;
		cout << endl;
		cout << mReasonShutdown << endl;
		break;

	default:
		break;
	}
}

void Robot::UpdateRobotState(const State& _state)
{
	string _display = "Leaving " + mEnumToString[(int)mRobotState] + " state..\n";
	mRobotState = _state;
	_display += "Now entering " + mEnumToString[(int)mRobotState] + " state..\n";
	cout << _display;
}

void Robot::LaunchRobot(const bool& _lauched)
{
	if(mbIsLaunched == _lauched) cout << "Robot is already " << (_lauched ? "ON" : "OFF") << endl;
	else cout << "Robot is now " << (_lauched ? "ON" : "OFF") << endl;
	mbIsLaunched = _lauched;
}

void Robot::AnalyzeEnvironnement()
{
	cout << "\n____________________________________________________________________\n";
	cout << "\nInitial environnement :\n\n";
	mWorkspace->DisplayStocks();
	mWorkspace->DisplayImportantSpots();
	mWorkspace->DisplayGrid(mCurrentPostion);
	GatherAllRawIngredients();
	cout << "____________________________________________________________________\n";
}

void Robot::DisplayReceipts() const
{
	for (unsigned int _indexReceipt = 0; _indexReceipt < mReceipts.size(); ++_indexReceipt)
		cout << _indexReceipt + 1 << ". " << mReceipts[_indexReceipt].DisplayReceipt();
	cout << endl;
}

bool Robot::ValidIndex(const int& _indexReceipt) const
{
	const bool _valid = (_indexReceipt > 0) && (_indexReceipt <= mReceipts.size());
	if (!_valid) cout << "Not valid, try again" << endl;
	return _valid;
}

void Robot::ChooseReceipt(const int& _indexReceipt)
{
	const int _realIndex = _indexReceipt - 1;
	mChoosenReceipt = mReceipts[_realIndex];
	cout << "You choose receipt number "<< _indexReceipt<< " : "<< mChoosenReceipt.GetName() << endl;
}

void Robot::GatherAllRawIngredients()
{
	map<string, Ingredient> _ingredients = mChoosenReceipt.GetAllRawIngredients();
	for (const pair<string, Ingredient>& _pair : _ingredients)
		mNeededIngredients.push_back(_pair.second);
	string _listIngredients = "\n";
	for (const Ingredient& _ingredient : mNeededIngredients)
		_listIngredients += _ingredient.ToString() + '\n';
	cout << "\nFor this receipt, the robot need : " << _listIngredients << endl;
}

void Robot::DoCurrentReceipt()
{
	const Step& _currentStep = mChoosenReceipt.GetCurrentStep();
	const vector<Ingredient>& _neededForCurrentStep = _currentStep.GetNeededIngredients();
	bool _canDoStep = true;
	for (const Ingredient& _ingredient : _neededForCurrentStep)
		_canDoStep &= QuantityOf(_ingredient.GetName()) >= _ingredient.GetQuantity();

	if (!_canDoStep)
	{
		cout << "Not enough ingredients.." << endl;
		DisplayIngredientsInStock();
		UpdateRobotState(State::SHUTTING_DOWN);
		return;
	}
	const Ingredient& _result = _currentStep.DoStep();
	RemoveFromRobotStock(_neededForCurrentStep);
	AddInRobotStock(_result, _result.GetQuantity());
	mChoosenReceipt.FinishStep();

	if (!mChoosenReceipt.ReceiptIsFinished() || mChoosenReceipt.GetName() != _result.GetName()) return;
	cout << "Receipt finished, " << _result.GetName() << " is ready to be served.\n" << endl;
	UpdateRobotState(State::SHUTTING_DOWN);
}

void Robot::ChooseNearestPosition(const string& _nameSpot)
{
	if (!mWorkspace)
	{
		mReasonShutdown = "/!\\Workspace not initialized/!\\";
		//throw exception("/!\\Workspace not initialized/!\\");
		UpdateRobotState(State::SHUTTING_DOWN);
		return;
	}

	const vector<Vec2>& _allPositionForSpot = mWorkspace->GetPositionsForSpot(_nameSpot);
	if (_allPositionForSpot.empty()) 
	{
		mReasonShutdown = "/!\\This spot doesn't exist/!\\";
		//throw exception("/!\\This spot doesn't exist/!\\");
		UpdateRobotState(State::SHUTTING_DOWN);
		return;
	}

	Vec2 _nearest = _allPositionForSpot[0];
	float _squaredDistanceNearest = Vec2::SquaredDistance(mCurrentPostion, _nearest);
	const unsigned int& _size = _allPositionForSpot.size();

	for (unsigned int _indexPosition = 1; _indexPosition < _size; ++_indexPosition)
	{
		const Vec2& _position = _allPositionForSpot[_indexPosition];
		const float& _squaredDistance = Vec2::SquaredDistance(mCurrentPostion, _position);
		if (_squaredDistance >= _squaredDistanceNearest) continue;
		_nearest = _position;
		_squaredDistanceNearest = _squaredDistance;
	}

	cout << "\nNearest spot for " << _nameSpot << " is at " << _nearest.DisplayVector() << endl;
	cout << endl;
	mTargetPosition = _nearest;
}

void Robot::MoveToTarget()
{
	if (mCurrentPostion == mTargetPosition) return;
	const Vec2& _vecDistance = mTargetPosition - mCurrentPostion;
	const int& _decalageLine = _vecDistance.X;
	const int& _decalageColumn = _vecDistance.Y;
	if (abs(_decalageLine) >= abs(_decalageColumn)) mCurrentPostion += Vec2(_decalageLine <= 0 ? -1 : 1, 0);
	else mCurrentPostion += Vec2(0, _decalageColumn <= 0 ? -1 : 1);
	if (!mWorkspace) return;
	mWorkspace->DisplayGrid(mCurrentPostion);
}

inline unsigned int Robot::QuantityOf(const string& _nameIngredient) const
{
	for (const pair<string, Ingredient>& _pair : mIngredientsInStock)
		if (_pair.first == _nameIngredient) return _pair.second.GetQuantity();
	return 0;
}

int Robot::TakeFromStock(Ingredient& _stock, const unsigned int& _quantity)
{
	const unsigned int& _stockQuantity = _stock.GetQuantity();
	const bool& _enoughQuantityInStock = _stockQuantity > _quantity;
	const unsigned int& _quantityTaken = _enoughQuantityInStock ? _quantity : _stockQuantity;
	_stock.DecreaseAmount(_quantityTaken);
	AddInRobotStock(_stock, _quantityTaken);
	cout << "\nTaken : " << _stock.GetName() << " x" << _quantityTaken << endl;
	cout << _stock.ToString() << " left." << endl;
	DisplayIngredientsLeftToCollect();
	cout << endl;
	return 0;
}

void Robot::AddInRobotStock(const Ingredient& _stock, const unsigned int& _quantity)
{
	const string& _name = _stock.GetName();
	Ingredient& _inStock = mIngredientsInStock[_name];
	if (_inStock.GetName().empty()) _inStock.SetName(_name);
	_inStock.IncreaseAmount(_quantity);
}

void Robot::RemoveFromRobotStock(const vector<Ingredient>& _list)
{
	for (const Ingredient& _ingredient : _list)
	{
		const string& _name = _ingredient.GetName();
		Ingredient& _inStock = mIngredientsInStock[_name];
		_inStock.DecreaseAmount(_ingredient.GetQuantity());
	}
}

void Robot::DisplayIngredientsInStock() const
{
	string _display = "Robot's stock :\n";
	for(const pair<string, Ingredient>& _pair : mIngredientsInStock)
		_display += _pair.second.ToString() + "\n";
	cout << _display << endl;
}

void Robot::DisplayIngredientsLeftToCollect() const
{
	string _display = "Robot still needs :\n";
	bool _stillIngredientsToCollect = false;
	for (const Ingredient& _ingredient : mNeededIngredients) 
	{
		const unsigned int& _quantity = _ingredient.GetQuantity() - QuantityOf(_ingredient.GetName());
		_stillIngredientsToCollect |= _quantity;
		_display += _ingredient.GetName() + " x" + to_string(_quantity) + "\n";
	}
	cout << _display << endl;
	if (!_stillIngredientsToCollect) cout << "Robot has everything, now looking to nearest " << WORKBENCH <<" available.." << endl;
}
