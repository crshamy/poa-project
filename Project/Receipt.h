#pragma once
#include<iostream>
#include<vector>
#include<map>
#include<string>
using namespace std;

#include "Step.h"

class Receipt
{
	string mName = "";
	vector<Step> mSteps = vector<Step>();
	unsigned int mCurrentStep = 0;

public:
	inline string GetName() const { return mName; }
	inline Step GetCurrentStep() const { return mCurrentStep >= mSteps.size() ? Step() : mSteps[mCurrentStep]; }
	inline bool ReceiptIsFinished() const { return mCurrentStep >= mSteps.size(); }
	inline void FinishStep() { ++mCurrentStep; }
	inline void ResetReceipt() { mCurrentStep = 0; }

public:
	Receipt();
	Receipt(const string& _name, const vector<Step>& _steps);

public:
	string DisplayReceipt() const;
	map<string, Ingredient> GetAllRawIngredients() const;

private:
	string DisplayListIngredients()const;
};

