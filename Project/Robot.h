#pragma once
#include <iostream>
#include <vector>
#include "Receipt.h"
#include "Vec2.h"

using namespace std;

class Grid;

enum class State
{
	PROPOSING,
	ANALYZING,
	SEARCHING,
	MOVING,
	GATHERING,
	COOKING,
	SHUTTING_DOWN
};

class Robot
{
	bool mbIsLaunched = false;
	State mRobotState = State::PROPOSING ;
	string mEnumToString[7] = {"PROPOSING", "ANALYZING" , "SEARCHING" , "MOVING" , "GATHERING" , "COOKING" , "SHUTTING_DOWN" };
	vector<Receipt> mReceipts = vector<Receipt>();
	Receipt mChoosenReceipt = Receipt();

	map<string, Ingredient> mIngredientsInStock = map<string, Ingredient>();
	vector<Ingredient> mNeededIngredients = vector<Ingredient>();
	unsigned int mIndexIngredientToSearch = 0;

	Vec2 mCurrentPostion = Vec2();
	Vec2 mTargetPosition = Vec2();

	Grid* mWorkspace = nullptr;

	string mReasonShutdown = "The receipt is finished, the robot is shutting down..";

public:
	inline void LinkWorkspace(Grid* const _workspace) { mWorkspace = _workspace; };

	inline State GetRobotState() const { return mRobotState; };

	inline Vec2 GetCurrentPosition() const { return mCurrentPostion; };
	inline void SetCurrentPosition(const Vec2& _currentPostion) { mCurrentPostion = _currentPostion; };

	inline Vec2 GetTargetPosition() const { return mTargetPosition; };
	inline void SetTargetPosition(const Vec2& _targetPostion) { mTargetPosition = _targetPostion; };

	inline bool HasEnough(const string& _name, const unsigned int& _quantity) const { return QuantityOf(_name) == _quantity; };
	inline bool IsON() const { return mbIsLaunched; };


public:
	Robot();
	Robot(const vector<Receipt>& _receipts);

public:
	void UpdateRobot();
	void UpdateRobotState(const State& _state);

	void LaunchRobot(const bool& _lauched);
	void AnalyzeEnvironnement();
	void DisplayReceipts() const;
	bool ValidIndex(const int& _indexReceipt) const;
	void ChooseReceipt(const int& _indexReceipt);

	void GatherAllRawIngredients();
	void DoCurrentReceipt();

	void ChooseNearestPosition(const string& _nameSpot);
	void MoveToTarget();

	unsigned int QuantityOf(const string& _nameIngredient) const;
	int TakeFromStock(Ingredient& _stock, const unsigned int& _quantity = 1);
	void AddInRobotStock(const Ingredient& _stock, const unsigned int& _quantity);
	void RemoveFromRobotStock(const vector<Ingredient>& _list);
	void DisplayIngredientsInStock() const;
	void DisplayIngredientsLeftToCollect() const;
};

