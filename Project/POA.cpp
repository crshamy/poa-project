#include "Robot.h"
#include "Grid.h"
#include <vector>

#include "Macro.h"

Receipt CreateReceiptA() 
{
	Step _step1 = Step( vector<Ingredient>	
						{
						Ingredient(SALAD, 1, true)
						},
						Mode::Slice,
						Ingredient(SLICED_SALAD, 5));

	Step _step2 = Step(vector<Ingredient>
						{
						Ingredient(TOMATO, 1, true)
						},
						Mode::Slice,
						Ingredient(SLICED_TOMATO, 5));

	Step _step3 = Step(vector<Ingredient>
						{
						Ingredient(OLIVE, 8, true),
						Ingredient(SLICED_SALAD, 5),
						Ingredient(SLICED_TOMATO, 5)
						},
						Mode::Mix,
						Ingredient(MIXED_SALAD_TOMATO_OLIVE, 1));


	Receipt _receiptA = Receipt(MIXED_SALAD_TOMATO_OLIVE,
						vector<Step>{ _step1, _step2, _step3});

	return _receiptA;
}

Receipt CreateReceiptB()
{
	Step _step1 = Step(vector<Ingredient>
	{
		Ingredient(SALAD, 1, true)
	},
		Mode::Slice,
		Ingredient(SLICED_SALAD, 5));

	Step _step2 = Step(vector<Ingredient>
	{
		Ingredient(TOMATO, 1, true)
	},
		Mode::Slice,
		Ingredient(SLICED_TOMATO, 5));

	Step _step3 = Step(vector<Ingredient>
	{
		Ingredient(ONION, 1, true)
	},
		Mode::Slice,
		Ingredient(SLICED_ONION, 5));


	Step _step4 = Step(vector<Ingredient>
	{
		Ingredient(MEAT, 1, true),
	},
		Mode::Slice,
		Ingredient(SLICED_MEAT, 5));

	Step _step5 = Step(vector<Ingredient>
	{
		Ingredient(SLICED_MEAT, 3),
	},
		Mode::Bake,
		Ingredient(BAKED_MEAT, 3));

	Step _step6 = Step(vector<Ingredient>
	{
		Ingredient(BREAD, 1, true),
		Ingredient(BAKED_MEAT, 3),
		Ingredient(SLICED_ONION, 5),
		Ingredient(SLICED_TOMATO, 5),
		Ingredient(SLICED_SALAD, 5),
	},
		Mode::Mix,
		Ingredient(KEBAB, 1));


	Receipt _receiptA = Receipt(KEBAB,
		vector<Step>{ _step1, _step2, _step3, _step4, _step5, _step6});

	return _receiptA;
}

void InitializeEnvironnementGrid(Grid& _workspaceGrid)
{
	try 
	{
		_workspaceGrid.SetImportantSpot(0, 0, WORKBENCH);
		_workspaceGrid.SetStock(1, 4, Ingredient(TOMATO, 3, true));
		_workspaceGrid.SetStock(3, 4, Ingredient(TOMATO, 12, true));
		_workspaceGrid.SetStock(4, 0, Ingredient(ONION, 5, true));
		_workspaceGrid.SetStock(4, 1, Ingredient(BREAD, 5, true));
		_workspaceGrid.SetStock(2, 2, Ingredient(OLIVE, 8, true));
		_workspaceGrid.SetStock(4, 4, Ingredient(SALAD, 5, true));
		_workspaceGrid.SetStock(2, 0, Ingredient(MEAT, 5, true));
	}
	catch (const out_of_range& _error) { cout << _error.what() << endl; }
}

vector<Receipt> CreateAvailableReceipts()
{
	vector<Receipt> _receipts{ CreateReceiptA(), CreateReceiptB()};
	return _receipts;
}

int main()
{
	Robot robot = CreateAvailableReceipts();
	robot.SetCurrentPosition(Vec2(0, 0));
	bool launched = false;
	
	//Launch robot
	cout << "Launch robot ? (0/1)" << endl;
	cin >> launched;
	robot.LaunchRobot(launched);
	if (!launched) return 0;

	//Choose receipt
	cout << "\nChoose a receipt among the above by entering the receipt's index :\n";
	int _indexChoosen = 0;
	do 
	{
		robot.DisplayReceipts();
		cin >> _indexChoosen;
	} 
	while (!robot.ValidIndex(_indexChoosen));
	robot.ChooseReceipt(_indexChoosen);
	robot.UpdateRobotState(State::ANALYZING);

	Grid workspaceGrid = SIZE_GRID;
	InitializeEnvironnementGrid(workspaceGrid);

	//Robot know its workspace
	robot.LinkWorkspace(&workspaceGrid);

	while (robot.IsON())
		robot.UpdateRobot();

	return 0;
}