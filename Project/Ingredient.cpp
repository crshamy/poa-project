#include "Ingredient.h"

Ingredient::Ingredient(){}

Ingredient::Ingredient(const Ingredient& _ingredient) : mName(_ingredient.GetName()), mQuantity(_ingredient.GetQuantity()), mbIsRawProduct(_ingredient.IsRaw()){}

Ingredient::Ingredient(const string& _name, const unsigned int& _quantity, const bool& _isRaw) : mName(_name) , mQuantity(_quantity), mbIsRawProduct(_isRaw){}

void Ingredient::IncreaseAmount(const unsigned int& _ammount)
{
    mQuantity += _ammount;
}

void Ingredient::DecreaseAmount(const unsigned int& _ammount)
{
    mQuantity -= _ammount;
}

string Ingredient::ToString() const
{
    return mName + " x" + to_string(mQuantity);
}
