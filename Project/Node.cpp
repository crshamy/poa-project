#include "Node.h"

#include <iostream>
using namespace std;

Node::Node(){}

Node::Node(const Vec2& _position, const string& _name) :  mPosition(_position), mName(_name){}

Node::Node(const unsigned int& _x, const unsigned int& _y, const string& _name) : mPosition(Vec2(_x, _y)), mName(_name) {}

string Node::DisplayNode() const
{
	string _node = "";
	return (mName.length() == 0 ? _node + BLANK_CHAR : _node+mName[0]);
}