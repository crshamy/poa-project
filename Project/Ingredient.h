#pragma once
#include<string>
using namespace std;

class Ingredient
{
	string mName = "";
	unsigned int mQuantity = 0;
	bool mbIsRawProduct = false;

public:
	inline string GetName() const { return mName; };
	inline void SetName(const string& _name) { mName = _name; };

	inline unsigned int GetQuantity() const { return mQuantity; };

	inline bool IsEmpty() const { return mQuantity == 0; };
	inline bool IsRaw() const { return mbIsRawProduct; };

public:
	Ingredient();
	Ingredient(const Ingredient& _ingredient);
	Ingredient(const string& _name, const unsigned int& _quantity, const bool& _isRaw = false);

public:
	void IncreaseAmount(const unsigned int& _ammount = 1);
	void DecreaseAmount(const unsigned int& _ammount = 1);
	string ToString() const;
};