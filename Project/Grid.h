#pragma once
#include <iostream>
#include <map>
#include <vector>
#include "Node.h"
#include "Macro.h"
using namespace std;

class Ingredient;

class Grid
{
	Node* mGrid = nullptr;
	unsigned int mDimension = 0;

	map<string, vector<Vec2>> mImportantSpots = map<string, vector<Vec2>>();
	map<unsigned int, Ingredient> mStocks = map<unsigned int, Ingredient>();

public:
	unsigned int GetInArrayPosition(const unsigned int& _line, const unsigned int& _column) const { return _line * mDimension + _column; };
	unsigned int GetInArrayPosition(const Vec2& _position) const { return _position.X * mDimension + _position.Y; };
	vector<Vec2> GetPositionsForSpot(const string& _nameSpot) { return (ContainsSpot(_nameSpot) ? mImportantSpots[_nameSpot] : vector<Vec2>()); };
	Ingredient* GetStockAtPos(const Vec2& _position) { const unsigned int& _pos = GetInArrayPosition(_position);  return (ContainsStock(_position) ? &mStocks[_pos] : nullptr); };
	string GetSpotAtPos(const Vec2& _position);

public:
	Grid(const unsigned int& _dimension);
	~Grid();


public:
	bool ContainsStock(const Vec2& _position) const;
	bool ContainsSpot(const string& _nameSpot) const;
	bool ContainsPosition(const Vec2& _position, const vector<Vec2>& _positions) const;

	void SetStock(const unsigned int& _line, const unsigned int& _column, const Ingredient& _ingredient);
	void ClearStockAtPos(const unsigned int& _line, const unsigned int& _column);

	void SetImportantSpot(const unsigned int& _line, const unsigned int& _column, const string& _spotName);
	void ClearSpotAtPos(const unsigned int& _line, const unsigned int& _column);

	void DisplayStocks() const;
	void DisplayImportantSpots() const;
	void DisplayGrid(const Vec2& _robotPosition) const;
};

