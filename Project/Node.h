#pragma once
#include <string>
#include "Vec2.h"
#include "Macro.h"
using namespace std;


class Node
{
	Vec2 mPosition = Vec2();
	string mName = "";

public:
	inline Vec2 GetPosition() const { return mPosition; }
	inline string GetName() const { return mName; }

public:
	Node();
	Node(const Vec2& _position, const string& _name = "");
	Node(const unsigned int& _x, const unsigned int& _y, const string& _name = "");

public:
	string DisplayNode() const;
};

