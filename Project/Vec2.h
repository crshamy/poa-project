#pragma once
#include <string>

using namespace std;

struct Vec2
{
	unsigned int X = 0, Y = 0;

	inline Vec2(const unsigned int& _x = 0, const unsigned int& _y = 0) : X(_x), Y(_y) {}

	/// <summary>
	/// Squared distance from A to B (vector B-A)
	/// </summary>
	/// <param name="_vectorA">: Vec2</param>
	/// <param name="_vectorB">: Vec2</param>
	/// <returns></returns>
	inline static float SquaredDistance(const Vec2& _vectorA, const Vec2& _vectorB) { return (_vectorB.X - _vectorA.X) * (_vectorB.X - _vectorA.X) + (_vectorB.Y - _vectorA.Y) * (_vectorB.Y - _vectorA.Y); };
	inline string DisplayVector() const { return "(" + to_string(X) + "," + to_string(Y) + ")"; }

	inline Vec2 operator+(const Vec2& _vec2) const { return Vec2(X + _vec2.X, Y + _vec2.Y); }
	inline Vec2 operator+=(const Vec2& _vec2) { return Vec2(X += _vec2.X, Y += _vec2.Y); }
	inline Vec2 operator-(const Vec2& _vec2) const { return Vec2(X - _vec2.X, Y - _vec2.Y); }
	inline Vec2 operator-=(const Vec2& _vec2) { return Vec2(X -= _vec2.X, Y -= _vec2.Y); }
	inline bool operator==(const Vec2& _vec2) const { return X == _vec2.X && Y == _vec2.Y; }
	inline bool operator!=(const Vec2& _vec2) const { return X != _vec2.X || Y != _vec2.Y; }
};

