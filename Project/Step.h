#pragma once
#include <iostream>
#include <vector>
#include "Ingredient.h"

enum class Mode
{
	Default,
	Slice,
	Bake,
	Mix
};

class Step
{
	vector<Ingredient> mIngredients = vector<Ingredient>();
	Mode mMode = Mode::Default;
	Ingredient mResult = Ingredient();

public:
	inline vector<Ingredient> GetNeededIngredients() const { return mIngredients; };
	vector<Ingredient> GetRawIngredients() const;

public:
	Step();
	Step(const vector<Ingredient>& _ingredients, const Mode& _mode, const Ingredient& _result);

public:
	Ingredient DoStep() const;
};

